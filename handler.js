import fs from 'fs';
import axios from 'axios';

import {
  SQSClient,
  ReceiveMessageCommand,
  DeleteMessageCommand,
} from '@aws-sdk/client-sqs';
import { SNSClient, PublishCommand } from '@aws-sdk/client-sns';

import { handleError } from './app/network/error.js';

const sqs = new SQSClient({ region: 'us-east-1' });
// const sns = new SNSClient({ region: 'us-east-1' });

/*******/
// async function callAPI(process) {
//   try {
//     // throw new Error(`Mensaje no contiene propiedad 'data'`);

//     // const processes = await fs.promises.readFile(
//     //   './app/store/processes.json',
//     //   'utf8'
//     // );

//     // const objProcess = JSON.parse(processes).find(
//     //   (obj) => obj.process === process
//     // );
//     // if (objProcess === undefined)
//     //   throw new Error(`El proceso no se encuentra registrado.`);

//     // console.log(objProcess);
//     // return;

//     await axios({
//       method: 'GET',
//       // url: 'https://pokeapi.co/api/v2/pokemon/dittox',
//       url: 'https://apidev.limatours.com.pe/api/v1/aventura/templates?offset=0&limit=100',
//       headers: {},
//       data: {
//         process: 'template.getAll',
//         data: {
//           parametro1: 'value1',
//           parametro2: 'value2',
//         },
//       },
//     });
//   } catch (error) {
//     // handleError(error);
//     handleError(error);
//   }
// }
// callAPI('template.getAll');
/*******/

// context
export const handler = async (event) => {
  const message = event.Records[0];

  try {
    await processMessage(message);
    await deleteMessage(message);

    return {
      statusCode: 200,
      body: JSON.stringify('Mensajes procesados correctamente'),
    };
  } catch (error) {
    handleError(error);
  }
};

async function receiveMessages() {
  console.log(process.env.SQS_QUEUE_URL);

  const command = new ReceiveMessageCommand({
    QueueUrl: process.env.SQS_QUEUE_URL,
    MaxNumberOfMessages: 10,
  });
  const response = await sqs.send(command);
  console.log(response);
  return response.Messages ?? [];
}

async function processMessage(message, retries = 0) {
  const { messageId, body } = message;
  const parsedBody = typeof body === 'string' ? JSON.parse(body) : body;

  if (!parsedBody.hasOwnProperty('process') || parsedBody.process.length === 0)
    throw new Error(`Mensaje no contiene propiedad 'process'`);

  if (!parsedBody.hasOwnProperty('data') || parsedBody.data.length === 0)
    throw new Error(`Mensaje no contiene propiedad 'data'`);

  await handleProcess(parsedBody);
}

async function handleProcess(message) {
  const { process, data } = message;

  const processes = await fs.promises.readFile(
    './app/store/processes.json',
    'utf8'
  );
  const objProcess = JSON.parse(processes).find(
    (obj) => obj.process === process
  );
  if (objProcess === undefined)
    throw new Error(`El proceso no se encuentra registrado.`);

  const { method, url } = objProcess;

  try {
    await axios({
      method,
      url,
      headers: {},
      data,
    });
  } catch (error) {
    throw new Error(error);
  }
}

async function deleteMessage(message) {
  const {
    messageId,
    receiptHandle: ReceiptHandle,
    eventSourceARN,
    awsRegion,
  } = message;

  let expARN = eventSourceARN.split(':');
  let QueueUrl = `https://sqs.${awsRegion}.amazonaws.com/${expARN[4]}/${expARN[5]}`;

  const params = {
    QueueUrl,
    ReceiptHandle,
  };

  const command = new DeleteMessageCommand(params);
  const result = await sqs.send(command);
  console.log(`Mensaje ${messageId} eliminado`);
  return true;
}

// async function publishNotification(error) {
//   const params = {
//     TopicArn: process.env.SNS_TOPIC_ARN,
//     Message: `Se ha producido un error al procesar mensajes de la cola de mensajes de SQS: ${error.message}`,
//     Subject: 'Error al procesar mensajes de SQS',
//   };

//   await sns.publish(params).promise();
//   console.log('Notificación de error publicada');
// }

async function sendNotification(success, message) {
  const { messageId } = message;

  let Subject = `Mensaje procesado: ${messageId}`;
  let Message = `Mensaje procesado correctamente: --}`;

  if (!success) {
    const { error } = message;
    Subject = `Error procesando mensaje: ${messageId}`;
    Message = `Error procesando mensaje: --} - ${error.message}`;
  }

  const params = {
    TopicArn: process.env.SNS_TOPIC_ARN,
    Subject,
    Message,
  };

  // await sns.publish(params).promise();
  // if (!success) console.log('Notificación de error publicada');
  // else console.log('Notificación de mensaje publicado');

  const command = new PublishCommand(params);
  try {
    const data = await sns.send(command);

    if (!success) console.log('Notificación de error publicada', data);
    else console.log('Notificación de mensaje publicado', data);
  } catch (error) {
    console.error('Error al enviar mensaje: ', error);
  }
}
