import axios from 'axios';

export const handleError = (error) => {
  console.log(typeof error);
  console.log(error);
  console.log(JSON.parse(error));

  // if (axios.isAxiosError(error)) {
  //   console.log('ERROR AXIOS');

  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log('error.response');
    console.log(error.response.status);
    if (error.response.status === 502) {
      console.log('Servicio caído... ');
    }
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log('error.request');
    console.log(error.response.status);
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log('error');
    console.log(
      'Something happened in setting up the request that triggered an Error'
    );
    console.log(error);
  }
  console.log('-------------------');
  console.log(error.config);
  console.log(error.config.method);
  console.log(error.config.url);
  console.log(error.config.data);
  console.log('-------------------');
  console.log(error.message);
  // } else {
  //   console.log('ERROR NO AXIOS');
  //   console.log(error.message);
  // }
};
